# Jenkins Jobs

**Configure Node**

1. go to `JENKINS_URL/configureTools`
    + Add NodeJS
        - default

**Add credentials**

1. go to `JENKINS_URL/credentials/store/system/domain/_/newCredentials`
1. Username with password

## Selenium Tests

1. Freestyle project
    + This project is parameterized
        - Choice
            + Name: `BRANCH`
            + Choices: `master, develop`
            + Description: `Select the branch to checkout.`
        - Choice
            + Name: `BROWSER`
            + Choices: `chrome, firefox, iphone, ipad, ie, internet_explorer, edge, safari`
            + Description: `SELENIUM_BROWSER=${BROWSER} npm run ${NPM_GOAL}`
        - String
            + Name: `NPM_GOAL`
            + Default: `test`
            + Description: `npm run ${NPM_GOAL}`
        - String
            + Name: `OPTIONS`
            + Description: `npm run ${NPM_GOAL} ${OPTIONS}` (e.g --ENV)
        - String
            + Name: `ARGUMENTS`
            + Description: `npm run ${NPM_GOAL} -- ${ARGUMENTS}` (e.g. --fgrep, --grep)
        - String
            + Name: `REMOTE`
            + Default: `http://username:accessKey@ondemand.saucelabs.com:80/wd/hub`
            + Description: `SELENIUM_REMOTE_URL=${REMOTE}`
1. Git
    + `https://repo.digiscloud.com/TCOM/tcom-apps.git`
    + Username with password
    + `origin/${BRANCH}`
1. Build Environment
    + Add timestamps to the Console Output
    + Provide Node & npm bin/ folder to PATH
        - NodeJS Installation: default
    + Set Build Name
        - `Selenium-#${BUILD_NUMBER}-${BUILD_STATUS}-${GIT_BRANCH}-${GIT_REVISION,length=8}`
1. Build
    + Execute shell
        ````
        cd application/ui
        rm -rf dist
        npm install
        SELENIUM_BROWSER=${BROWSER} SELENIUM_REMOTE_URL=${REMOTE} npm run ${NPM_GOAL} ${OPTIONS} -- ${ARGUMENTS}
        ````
1. Post-build Actions
    + Archive the artifacts
        - `application/ui/dist/archive/**`

## TCOM-APPS

1. Freestyle project
    + Discard old builds
        - 14 days
        - 100 max builds
    + This project is parameterized
        - Extended Choice
            + Name: `APP_NAME`
            + Description: `Select one or more apps.`
        - String
            + Name: `APP_NAME_OVERRIDE`
            + Description: `Specify an app or apps not listed above.`
        - Choice
            + Name: `BRANCH`
            + Choices: `master, develop`
            + Description: `Select the branch to checkout.`
        - Choice
            + Name: `TARGET_ENVIRONMENT`
            + Choices: `dev1, dev2, dev3, dev4, dev5, test1, test2, test3, test4, test5`
            + Description: `Select the target deploy environment.`
        - String
            + Name: `NPM_GOAL`
            + Default: `build`
            + Description: `npm run ${NPM_GOAL}`
        - String
            + Name: `NPM_FLAGS`
            + Default: `--production`
            + Description: `npm run ${NPM_GOAL} ${NPM_FLAGS}`
1. Git
    + `https://repo.digiscloud.com/TCOM/tcom-apps.git`
    + Username with password
1. Build Environment
    + Add timestamps to the Console Output
    + Provide Node & npm bin/ folder to PATH
        - NodeJS Installation: `default`
    + Set Build Name
        - `TCOM-APPS-#${BUILD_NUMBER}-${BUILD_STATUS}-${GIT_BRANCH}-${GIT_REVISION,length=8}-${TARGET_ENVIRONMENT}`
1. Build
    + Execute shell
        ````
        cd application/ui
        npm install
        npm run ${GOAL}
        ````
1. Post-build Actions
    + Archive the artifacts
        - `application/ui/dist/tcom-apps/**`